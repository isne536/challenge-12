#include<iostream>
#include<list>
#include<string>

using namespace std;

class students
{
	public:
	int ID;
 	string name;
};

void H_ID(list<students>& people, int id, string name);    //calculate id of store.
int intHash(int id);	//hash function convert id.
void H_Name(list<students> people[50], int id, string name);   //function convert name.
int strHash(string toHash, const int TableSize);     //calculate position of index.
string lookName(list<students> people[50], int id);      //function which allows you to look up a students name based on their id.
int lookID(list<students> people[50], string text);     ////function which allows you to look up a students id based on their name.

int main() 
{
 	list<students> people1[50], people2[50];
 	string name;
	int code,to_string;

 	/*input and hash ID  of students */
 	for (int i = 0; i < 50; i++) H_ID(people1[i], 560611001 + i, to_string(5600 + i + 1)); 
 	for (int i = 0; i < 50; i++) H_ID(people1[i], 570611001 + i, to_string(5700 + i + 1));
 	for (int i = 0; i < 50; i++) H_ID(people1[i], 580611001 + i, to_string(5800 + i + 1));
 	for (int i = 0; i < 50; i++) H_ID(people1[i], 590611001 + i, to_string(5900 + i + 1));
 	for (int i = 0; i < 50; i++) H_ID(people1[i], 600611001 + i, to_string(6000 + i + 1));
 
 	/*input and hash name  of students */
 	for (int i = 0; i < 50; i++) H_Name(people2, 560611001 + i, to_string(5600 + i + 1));
 	for (int i = 0; i < 50; i++) H_Name(people2, 570611001 + i, to_string(5700 + i + 1));
 	for (int i = 0; i < 50; i++) H_Name(people2, 580611001 + i, to_string(5800 + i + 1));
 	for (int i = 0; i < 50; i++) H_Name(people2, 590611001 + i, to_string(5900 + i + 1));
 	for (int i = 0; i < 50; i++) H_Name(people2, 600611001 + i, to_string(6000 + i + 1));
 
 	/*test look data*/
 	cout << "Enter ID for seach name : ";
 	cin >> code;  //input ID 
 	name = lookName(people1,code);       //search ID form name
 	cout << "Name is : " << name << endl;     //output name
 	cout << "Enter name for seach ID : "; cin >> name;  //input name
 	code = lookID(people2,name);       //search name form ID
 	cout << "ID is : " << code << endl;      //output ID 
 	system("pause");
 	return 0;
}


void H_ID(list<students> &people, int id, string name) 
{
 	students temp; 
	temp.ID = intHash(id)-1;  //set values and hash id.
 	temp.name = name;    //set name.
 	people.push_back(temp);   //push 
}


int intHash(int id) 
{
	return ((id/10000000)*100)+(id % 100); //calculate hash ID;
}

void H_Name(list<students> people[50], int id, string name) 
{
  	students temp;
  	int pos;
  	temp.ID = id;      //set values
  	temp.name = name;     //set values
  	pos = strHash(name,50);    //call funtion to find position
  	people[pos].push_back(temp);  //push
}

int strHash(string toHash, const int TableSize) 
{
 	int hashValue = 0;
 	/*calculate ascii code*/
 	for (unsigned int Pos = 0; Pos < toHash.length(); Pos++) hashValue = hashValue + int(toHash.at(Pos));   //sum ascii
 	return (hashValue % TableSize);                     //hashValue / size arry
}


string lookName(list<students> people[50], int id) 
{
 	students temp_struct;
 	list<students>::iterator it = people[(id % 100) - 1].begin(); //point iterator to 2 last code.
 	/*move linklist by use iterator */
 	for (int i = 1; i <= (id / 10000000) - 56; i++) it++; //year 55 run for 0 time ,56 run for 1 time....n(years -55 = time to run for)   
 	temp_struct = *it;   //return values
 	return temp_struct.name;
}

int lookID(list<students> people[50], string text) 
{
 	students temp;
 	int num = strHash(text, 50);       //hash to find position in index.     
 	list<students>::iterator it = people[num].begin();      //strat in index to values stay in lisklist.
 	temp = *it;            //stroe values to check in loop.
 	while (text != temp.name)        //run linklist.
	{
  		it++;            //move next elements.
  		temp = *it;           //stroe values to check in loop.
 	}
 	return temp.ID; //return ID
}
